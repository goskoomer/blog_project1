from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic


# Мы создаем подкласс общего представления на основе классов CreateView в нашем классе SignUpView.
# Мы указываем использование встроенного UserCreationForm и еще не созданного шаблона
# И мы используем reverse_lazy, чтобы перенаправить пользователя на страницу входа в систему после
# успешная регистрация.
# Зачем здесь использовать reverse_lazy вместо reverse? Причина в том, что для всех базовых классов
# просматривает URL-адреса не загружаются при импорте файла, поэтому мы должны использовать
# ленивая форма обратного, чтобы загрузить их позже, когда они будут доступны.
# Теперь давайте добавим signup.html в наши шаблоны уровня проекта.

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


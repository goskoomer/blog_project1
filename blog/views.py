from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import *


# Create your views here.

# BuBodum spusok postov
class BlogListView(ListView):
    model = Post
    template_name = 'home.html'


# delaem sulky , dla perexoda na post
# В этом новом представлении мы определяем модель, которую мы используем, Post и шаблон, который мы хотим
# это связано с, post_detail.html. По умолчанию DetailView предоставит контекст
# объект, который мы можем использовать в нашем шаблоне, который называется либо объект, либо имя в нижнем регистре
# наша модель, пост. Кроме того, DetailView ожидает либо первичный ключ, либо слаг, переданный ему
# в качестве идентификатора.
class BlogDetailView(DetailView):
    model = Post
    template_name = 'post_detail.html'


class BlogCreateView(CreateView):
    model = Post
    template_name = 'post_new.html'
    fields = '__all__'


class BlogUpdateView(UpdateView):
    model = Post
    fields = ['title', 'body']
    template_name = 'post_edit.html'


class BlogDeleteView(DeleteView):
    model = Post
    template_name = 'post_delete.html'
    success_url = reverse_lazy('home')

# Мы используем reverse_lazy вместо просто reverse, чтобы он не выполнял URL
# перенаправлять до тех пор, пока наше представление не завершит удаление сообщения блога.
# Наконец, добавьте URL, импортировав наше представление BlogDeleteView и добавив новый шаблон.

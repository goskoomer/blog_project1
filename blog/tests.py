from django.test import TestCase, Client

from django.contrib.auth import get_user_model
from django.urls import reverse

from .models import Post


class BlogTests(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='testuser',
            email='test@email.com',
            password='secret',
        )

        self.post = Post.objects.create(
            title='A good title',
            body='Nice body content',
            author=self.user,
        )

    def test_string_representation(self):
        post = Post(title='A sample title')
        self.assertEqual(str(post), post.title)

    def test_get_absolute_url(self):
        self.assertEquals(self.post.get_absolute_url(), '/post/1/')

    def test_post_content(self):
        self.assertEqual(f'{self.post.title}', 'A good title')
        self.assertEqual(f'{self.post.author}', 'testuser')
        self.assertEqual(f'{self.post.body}', 'Nice body content')

    def test_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Nice body content')
        self.assertTemplateUsed(response, 'home.html')

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/100000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'A good title')
        self.assertTemplateUsed(response, 'post_detail.html')

    def test_post_create_view(self):
        response = self.client.post(reverse('post_new'), {
            'title': 'New title',
            'body': 'New text',
            'author': self.user,
        })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'New title')
        self.assertContains(response, 'New text')

    def test_post_update_view(self):
        response = self.client.post(reverse('post_edit', args='1'), {
            'title': 'Updated title',
            'body': 'Updated text',
        })

        self.assertEqual(response.status_code, 302)

    def test_post_delete_view(self):
        response = self.client.get(
            reverse('post_delete', args='1'))

        self.assertEqual(response.status_code, 200)

# В этих тестах есть много нового, поэтому мы пройдемся по ним медленно. На вершине
# мы импортируем get_user_model для ссылки на нашего активного пользователя. Мы импортируем TestCase, который
# мы видели раньше, а также Client (), который является новым и используется в качестве фиктивного веб-браузера
# для имитации запросов GET и POST по URL. Другими словами, всякий раз, когда вы
# Для тестирования представлений вы должны использовать Client ().
# В нашем методе setUp мы добавляем образец сообщения в блог для проверки, а затем подтверждаем, что оба его
# строковое представление и содержание правильные. Затем мы используем test_post_list_view для
# подтвердите, что наша домашняя страница возвращает код состояния HTTP 200, содержит наш основной текст,
# и использует правильный шаблон home.html. Наконец test_post_detail_view проверяет, что наш
# страница сведений работает, как и ожидалось, и что неправильная страница возвращает 404. Это всегда
# хорошо, чтобы проверить, что что-то существует, а что-то неправильное не
# существуют в ваших тестах


# Мы ожидаем, что URL нашего теста будет на post /   /, так как есть только один пост и
# это его первичный ключ, который Django добавляет автоматически. Чтобы проверить создание представления, мы делаем
# новый ответ, а затем убедитесь, что ответ проходит (код состояния 200) и
# содержит наш новый заголовок и основной текст. Для просмотра обновлений мы получаем доступ к первому сообщению, которое
# имеет pk из  , который передается в качестве единственного аргумента - и мы подтверждаем, что это приводит
# в редиректе 302. Наконец, мы проверяем наш вид удаления, подтверждая, что если мы удалим сообщение
# код состояния 200 для успеха.
# Всегда можно добавить больше тестов, но это, по крайней мере, распространяется на все наши
# новый функционал
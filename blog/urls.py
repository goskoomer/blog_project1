from django.urls import path

from .views import *

urlpatterns = [
    path('', BlogListView.as_view(), name='home'),
    path('post/<int:pk>/', BlogDetailView.as_view(), name='post_detail'),
    path('post/new/', BlogCreateView.as_view(), name='post_new'),
    path('post<int:pk>edit/', BlogUpdateView.as_view(), name='post_edit'),
    path('post/<int:pk>/delete/', BlogDeleteView.as_view(), name='post_delete'),

]

# Все записи в блоге начнутся с записи /. Далее основной ключ для нашей записи
# который будет представлен как целое число <int: pk>. Какой первичный ключ ты
# наверное спрашиваешь? Django автоматически добавляет автоинкрементный первичный ключ к нашему
# модели базы данных. Так что пока мы только объявили поля title, author и body на нашем
# Пост-модель, под капотом Django также добавила еще одно поле под названием id, которое является нашим
# основной ключ. Мы можем получить к нему доступ либо как id, либо как pk.
# ПК для нашего первого поста «Hello, World» - 1. Для второго поста - 2. И так далее.
# Поэтому, когда мы переходим к отдельной странице входа для нашего первого сообщения, мы можем ожидать
# что его urlpattern будет post
